input_data = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,13,19,2,9,19,23,1,23,6,27,1,13,27,31,1,31,10,35,1,9,35,39,1,39,9,43,2,6,43,47,1,47,5,51,2,10,51,55,1,6,55,59,2,13,59,63,2,13,63,67,1,6,67,71,1,71,5,75,2,75,6,79,1,5,79,83,1,83,6,87,2,10,87,91,1,9,91,95,1,6,95,99,1,99,6,103,2,103,9,107,2,107,10,111,1,5,111,115,1,115,6,119,2,6,119,123,1,10,123,127,1,127,5,131,1,131,2,135,1,135,5,0,99,2,0,14,0]

def get_result(code, value_one, value_two):
    if code == 1:
        return value_one + value_two
    elif code == 2:
        return value_one * value_two

def run_sub_program(data_set, noun, verb):
    data_set[1] = noun
    data_set[2] = verb

    index = 0
    code = -1

    while code != 99:
        current_program = data_set[index : index + 4]

        code = current_program[0]

        if code != 99:
            first_value = data_set[current_program[1]]
            second_value = data_set[current_program[2]]
            output_position = current_program[3]

            val = get_result(code, first_value, second_value)
            data_set[output_position] = val
            
            index += 4

    return data_set[0]

def run_program():
    for noun in range(0, 100):
        for verb in range(0, 100):
            data = input_data[:]
            val = run_sub_program(data, noun, verb)
            if val == 19690720:
                print(str(noun) + " | " + str(verb))


run_program()